using Comonicon
import YAML

include("src/manage.jl")
include("src/model.jl")

#registration of packages
include("src/data/gt/gt_fit.jl")
include("src/data/lfc/lfc_fit.jl")
include("src/data/be/be_fit.jl")
include("src/data/fe/fe_fit.jl")

fits = Dict(
    "lfc" => fit_lfc, 
    "gt" => fit_gt,
    "be" => fit_be,
    "fe" => fit_fe,
)

const greeting = 
"""
# USAGE
The program for computing of the rapidly rotatated NS

Used the code for interpolation of the spectral fiting parameters,
the color correction factor f_c and the spectral dilution factor w
"""

const theoretical_notes = """
# THEORY 
* [data 13.03.2024]

Used the code for interpolation of the spectral fiting parameters,
the color correction factor f_c and the spectral dilution factor w
    
--> w ~ f_c^(-4)

The hot NS model atmosphere spectrum is approximated with the diluted blackbody

--> F_E(T_eff) ~ w*/pi*B_E(T_c=f_c*T_eff).

Here T_eff is the effective temperature of the model atmosphere, determined as

--> F_bol=/int _0^/infty F_E dE = /sigma_SB*(T_eff)^4

Here /sigma_SB = 5.67 10^{-5} is the Stefan-Boltzmann constant.
The approximation is good enough for the photon energy range 3-20 keV
in the observer's frame.

The input parameters is the chemical composition of the atmosphere,
the surface log g (must be between 13.7 and 14.9), and the effective
temperature, or the relative luminosity /ell = L/L_Edd =(T_eff/T_Edd)^4,
where (T_Edd)^4 = c * 10^{log g}/(/sigma_SB*/sigma_T),
c is the light speed, /sigma_T = 0.2(1+X) cm^2 g^{-1} is the
Thomson electron scattering opacity, X is the hydrogen mass fraction.

The values f_c and w are tabulated for three chemical compositions:
pure helium, solar composition, solar H/He mix with the heavy element
abundances reduced in 100 times, for 9 /log g and 23 /ell (from 0.1 to 1.12).

The input parameters are log g = "glg1" and the "flump".
The latest parameter could be the relative luminosity /ell,
or T_eff in Kelvin or in keV

The output parameters are w - "ww1" dilution factor,
and T_c = f_c * T_eff - "tc1" color temperature in keV

# REFERENCES
= Suleimanov et al 2011, A&A, 527, A139
= Suleimanov et al 2012, A&A, 545, A120
= Suleimanov et al 2017, MNRAS, 466, 906
* [data 31.10.2020]
"""

"""
Program CLI.

# Options
- `-t, --type=<str>`: working with one chemical composition
    * type = s001 - solar H/He mix with the heavy element abundances reduced in 100 times
    * type = s1   - solar abundance
    * type = he   - pure helium

- `-d, --dim=<str>`: working with one dependence
    * dim = lfc - lumen to f_c dependence
    * dim = gt  - g/g_0 to θ dependence
    * dim = be  - B_planck to ε dependence
    * dim = fe  - normalized flux to E/E' dependence

- `-s, --sys=<str>`: working with one configuration
    * sys = base - base config
    * sys = fig_a1 - config for figure A.1 in paper arXiv:2005.09759
    * sys = line - config for testing line-dependence of g_eff from θ

- `-g, --graph=<str>`: printing additional elements
    * graph = all  - print all chemical composion
    * graph = edge - apply edge-line to graph

- `-w, --wkey=<str>`: profile of latitudinal velocity, normalized to keplerian velocity
    * w-key = null  - without spread belt
    * w-key = const - decreasion of g_null
    * w-key = line  - firts model of latitudinal velocity

- `--kfc=<int>`: fc-key is the key for the tabulated fitting parameters w and f_c obtaining
    * fc-key = 1 - the energy spectra F_E were fitted
    * fc-key = 2 - the photon spectra F_E / E were fitted

- `--kfl=<int>`: flux-key is the relative or absolute flux over the surface of a neutron star
    * flux-key = 1 - relative flux F/F_Edd
    * flux-key = 2 - absolute flux F
    
- `-m, --mns=<int>`: mass of neutron star, in solar masses
- `-r, --rns=<int>`: radius of neutron star, in km
- `-v, --vrot=<int>`: rotation frequency of neutron star, in Hz
- `-i, --iang=<int>`: inclination angle between the axis of rotation and the line of sight, in deg
- `--mcor=<int>`: corrected mass of neutron star, in solar masses
- `--req=<int>`: equatorial radius of neutron star, in km

- `--theta=<int>`: angle of latitude, in deg
- `--phi=<int>`: angle of longitude, in deg
- `--lum=<int>`: relative luminosity, in L_edd

# Flags 
- `-p, --par`: save parameters of neutron star
"""
@cast function calc(;dim::String="lfc", sys::String="base", type::String="", graph::String="", wkey::String="", kfc::Int=0, kfl::Int=0, mns::Float64 = 0.0, mcor::Float64=0.0, rns::Float64=0.0, req::Float64=0.0, vrot::Float64=0.0, iang::Float64=0.0, theta::Float64=0.0, phi::Float64=0.0, lum::Float64=0.0, par::Bool=false)
    args = Dict(
        "par" => par,
        "type" => type,
        "w_key" => wkey,
        "kfc" => kfc,
        "kfl" => kfl,
        "m_ns" => mns,
        "m_cor" => mcor,
        "r_ns" => rns,
        "r_eq" => req,
        "v_rot" => vrot,
        "i_ang" => iang,
        "theta" => theta,
        "phi" => phi,
        "lum" => lum,
        "graph" => graph,
    )
    calculate(dim, sys, args) 
    draw(dim, sys, args)
end

"""
Program CLI.

# Options
- `-t, --type=<str>`: working with one chemical composition
    * type = s001 - solar H/He mix with the heavy element abundances reduced in 100 times
    * type = s1   - solar abundance
    * type = he   - pure helium

- `-d, --dim=<str>`: working with one parameters
    * dim = lfc - lumen to f_c dependence
    * dim = gt  - g/g_0 to θ dependence
    * dim = enu  - E to ν dependence

- `-s, --sys=<str>`: working with one parameters
    * sys = base - base config
    * sys = fig_a1 - config for figure A.1 in paper arXiv:2005.09759
    * sys = line - config for testing line-dependence of g_eff from θ

- `-g, --graph=<str>`: working with one parameters
    * graph = all  - print all chemical composion
    * graph = edge - apply edge to graph
"""
@cast function print(;dim::String="lfc", sys::String="base", type::String="", graph::String="")
    args = Dict(
        "type" => type,
        "graph" => graph,
    )
    draw(dim, sys, args)
end

"""
Program CLI.

# Flags 
- `--theory`: printing the theoretical notes
"""
@cast function guide(;theory::Bool=false)
    if theory
        println(theoretical_notes)
    else
        println(greeting)
    end
end

@main