using Plots
using UnicodePlots: lineplot, lineplot!
using LaTeXStrings
using Printf

function calculate(dim, sys, args)
    parameters = YAML.load_file("src/config/$sys.yml")
    type = args["type"] == "" ? parameters["fit_par"]["type"] : args["type"]
    X, Y = sp_rot_NS(dim, parameters, args)

    data = Dict(
        "Info" => Dict(
            "name" => dim,
            "config" => sys,
            "type" => type,
        ),
        "Data" => Dict(
            "X" => X,
            "Y" => Y,
        )
    )
    YAML.write_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).yml", data)
end

function draw(dim, sys, args)
    #upload data
    parameters = YAML.load_file("src/config/$sys.yml")
    graph = args["graph"] == "" ? parameters["fit_par"]["graph"] : args["graph"]

    if graph=="base"
        type = args["type"] == "" ? parameters["fit_par"]["type"] : args["type"]
        data = YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).yml")
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "s001" => 1,
            "s1" => 2,
            "he" => 3,
        )
        X = data["Data"]["X"]
        Y = data["Data"]["Y"]

        kch = Chemical_Composion[type]
        color = [:red, :blue, :green]
        name = ["H/He mix", "solar abundance", "pure He"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        space = ""
        for k=1:(16-length(xlabel)) space *= " " end
        println("\n   N     $(xlabel)$space$(ylabel)")
        N = length(X)
        for k=1:N
            println(@sprintf("%4i  %14.5e  %14.5e", k, X[k], Y[k]))
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = X[1], X[end]
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            ay, by = minimum(Y), maximum(Y)
        end

        plt = lineplot(X, Y, title=title, name=name[kch], color=color[kch], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        println(plt)

        #draw the graph
        plot(X, Y; seriestype=:path, lc=color[kch], label=name[kch], minorgrid=true, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end

        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))
        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).pdf") 

    elseif graph=="log"
        type = args["type"] == "" ? parameters["fit_par"]["type"] : args["type"]
        data = YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).yml")
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "s001" => 1,
            "s1" => 2,
            "he" => 3,
        )
        X = data["Data"]["X"]
        Y = data["Data"]["Y"]

        kch = Chemical_Composion[type]
        color = [:red, :blue, :green]
        name = ["H/He mix", "solar abundance", "pure He"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        space = ""
        for k=1:(16-length(xlabel)) space *= " " end
        println("\n   N     $(xlabel)$space$(ylabel)")
        N = length(X)
        for k=1:N
            println(@sprintf("%4i  %14.5e  %14.5e", k, X[k], Y[k]))
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = X[1], X[end]
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            ay, by = minimum(Y), maximum(Y)
        end

        plt = lineplot(X, Y, xscale=:log10, yscale=:log10, title=title, name=name[kch], color=color[kch], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        println(plt)

        #draw the graph
        plot(X, Y; xscale=:log10, yscale=:log10, seriestype=:path, lc=color[kch], label=name[kch], minorgrid=true, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end

        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))
        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).pdf") 

    elseif graph=="all"
        type = ["s1", "s001", "he"]
        data = [YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type[i]).yml") for i=1:3]
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "s001" => 1,
            "s1" => 2,
            "he" => 3,
        )
        X = [data[i]["Data"]["X"] for i=1:3]
        Y = [data[i]["Data"]["Y"] for i=1:3]

        kch = [Chemical_Composion[type[i]] for i=1:3]
        color = [:red, :blue, :green]
        name = ["H/He mix", "solar abundance", "pure He"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        for i=1:3
            println("\nFor chemical composion $(type[i]):")
            space = ""
            for k=1:(16-length(xlabel)) space *= " " end
            println("   N     $(xlabel)$space$(ylabel)")
            M = length(X[i])
            for k=1:M
                println(@sprintf("%4i  %14.5e  %14.5e", k, X[i][k], Y[i][k]))
            end
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = minimum([X[i][1] for i=1:3]), maximum([X[i][end] for i=1:3])
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            Y_all = Y[1]
            for i=2:3
                Y_all = hcat(Y_all,Y[i])
            end
            ay, by = minimum(Y_all), maximum(Y_all)
        end

        plt = lineplot(X[1], Y[1], title=title, name=name[kch[1]], color=color[kch[1]], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        for i=2:3
            lineplot!(plt, X[i], Y[i], name=name[kch[i]], color=color[kch[i]])
        end
        println(plt)

        #draw the graph
        plot(X[1], Y[1]; seriestype=:path, lc=color[kch[1]], label=name[kch[1]], minorgrid=true, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end
        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))

        for i=2:3
            plot!(X[i], Y[i]; seriestype=:path, lc=color[kch[i]], label=name[kch[i]],)
        end

        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_all.pdf") 

    elseif graph=="allog"
        type = ["s1", "s001", "he"]
        data = [YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type[i]).yml") for i=1:3]
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "s001" => 1,
            "s1" => 2,
            "he" => 3,
        )
        X = [data[i]["Data"]["X"] for i=1:3]
        Y = [data[i]["Data"]["Y"] for i=1:3]

        kch = [Chemical_Composion[type[i]] for i=1:3]
        color = [:red, :blue, :green]
        name = ["H/He mix", "solar abundance", "pure He"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        for i=1:3
            println("\nFor chemical composion $(type[i]):")
            space = ""
            for k=1:(16-length(xlabel)) space *= " " end
            println("   N     $(xlabel)$space$(ylabel)")
            M = length(X[i])
            for k=1:M
                println(@sprintf("%4i  %14.5e  %14.5e", k, X[i][k], Y[i][k]))
            end
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = minimum([X[i][1] for i=1:3]), maximum([X[i][end] for i=1:3])
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            Y_all = Y[1]
            for i=2:3
                Y_all = hcat(Y_all,Y[i])
            end
            ay, by = minimum(Y_all), maximum(Y_all)
        end

        plt = lineplot(X[1], Y[1], xscale=:log10, yscale=:log10, title=title, name=name[kch[1]], color=color[kch[1]], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        for i=2:3
            lineplot!(plt, X[i], Y[i], name=name[kch[i]], color=color[kch[i]])
        end
        println(plt)

        #draw the graph
        plot(X[1], Y[1]; seriestype=:path, xscale=:log10, yscale=:log10, lc=color[kch[1]], label=name[kch[1]], minorgrid=false, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end
        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))

        for i=2:3
            plot!(X[i], Y[i]; seriestype=:path, lc=color[kch[i]], label=name[kch[i]],)
        end

        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_all.pdf") 

    elseif graph=="iallog"
        type = ["i0", "i90"]
        data = [YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type[i]).yml") for i=1:2]
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "i90" => 1,
            "i0" => 2,
        )
        X = [data[i]["Data"]["X"] for i=1:2]
        Y = [data[i]["Data"]["Y"] for i=1:2]

        kch = [Chemical_Composion[type[i]] for i=1:2]
        color = [:red, :blue, :green]
        name = ["i = 90°", "i = 0°"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        for i=1:2
            println("\nFor chemical composion $(type[i]):")
            space = ""
            for k=1:(16-length(xlabel)) space *= " " end
            println("   N     $(xlabel)$space$(ylabel)")
            M = length(X[i])
            for k=1:M
                println(@sprintf("%4i  %14.5e  %14.5e", k, X[i][k], Y[i][k]))
            end
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = minimum([X[i][1] for i=1:2]), maximum([X[i][end] for i=1:2])
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            Y_all = Y[1]
            for i=2:2
                Y_all = hcat(Y_all,Y[i])
            end
            ay, by = minimum(Y_all), maximum(Y_all)
        end

        plt = lineplot(X[1], Y[1], xscale=:log10, yscale=:log10, title=title, name=name[kch[1]], color=color[kch[1]], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        for i=2:2
            lineplot!(plt, X[i], Y[i], name=name[kch[i]], color=color[kch[i]])
        end
        println(plt)

        #draw the graph
        plot(X[1], Y[1]; seriestype=:path, xscale=:log10, yscale=:log10, lc=color[kch[1]], label=name[kch[1]], minorgrid=true, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end
        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))

        for i=2:2
            plot!(X[i], Y[i]; seriestype=:path, lc=color[kch[i]], label=name[kch[i]],)
        end

        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_iall.pdf") 



    elseif graph=="edge"
        type = args["type"] == "" ? parameters["fit_par"]["type"] : args["type"]
        data = YAML.load_file("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_$(type).yml")
        data_edge = YAML.load_file("src/data/$(dim)/res/$(dim)_edge.yml")
        Graph = YAML.load_file("src/data/$(dim)/graph_config.yml")
        Chemical_Composion = Dict(
            "s001" => 1,
            "s1" => 2,
            "he" => 3,
        )
        X = [data["Data"]["X"], data_edge["Data"]["X"]]
        Y = [data["Data"]["Y"], data_edge["Data"]["Y"],]

        kch = Chemical_Composion[type]
        color = [:red, :blue, :green]
        name = ["H/He mix", "solar abundance", "pure He"]
        title = Graph["title"]
        xlabel = Graph["xlabel"]
        ylabel = Graph["ylabel"]

        # draw in table
        space = ""
        for k=1:(16-length(xlabel)) space *= " " end
        println("\n   N     $(xlabel)$space$(ylabel)")
        K = length(X[1])
        for k=1:K
            println(@sprintf("%4i  %14.5e  %14.5e", k, X[1][k], Y[1][k]))
        end
        println("")

        # draw in cmd
        if length(Graph["xlims"])!=1
            ax, bx = Graph["xlims"]
        else
            ax, bx = X[1][1], X[1][end]
        end 
        if length(Graph["ylims"])!=1
            ay, by = Graph["ylims"]
        else
            ay, by = minimum(Y[1]), maximum(Y[1])
        end

        plt = lineplot(X[1], Y[1], title=title, name=name[kch], color=color[kch], xlabel=xlabel, ylabel=ylabel, xlim=(ax, bx), ylim = (ay, by))
        lineplot!(plt, X[2], Y[2], name="edge", color=:black)
        println(plt)

        #draw the graph
        plot(X[1], Y[1]; seriestype=:path, lc=color[kch], label=name[kch], minorgrid=true, size=(677,565))
        if length(Graph["xlims"])!=1
            a, b = Graph["xlims"]
            xlims!(a, b)
        end
        if length(Graph["ylims"])!=1
            a, b = Graph["ylims"]
            ylims!(a, b)
        end
        xlabel!(latexstring(Graph["Lxlabel"]))
        ylabel!(latexstring(Graph["Lylabel"]))
        title!(latexstring(Graph["Ltitle"]))
        plot!(X[2], Y[2]; seriestype=:path, lc=:black, label="edge",)

        savefig("src/data/$(dim)/res/$(sys)/$(dim)_$(sys)_edge.pdf") 
    end
end