function fit_be(args)
# Определение массивов
θ = Array{Float64}(undef, 2000)
W = Array{Float64}(undef, 2000)
R_θ = Array{Float64}(undef, 2000)
cos_η = Array{Float64}(undef, 2000)
dS_φ = Array{Float64}(undef, 2000)
sin_η = Array{Float64}(undef, 2000)

grv = Array{Float64}(undef, 2000)
φ = Array{Float64}(undef, 2000)
dR_θ = Array{Float64}(undef, 2000)
β_ph = Array{Float64}(undef, 2000)

γ = Array{Float64}(undef, 2000)
δ = Array{Float64}(undef, 2000, 2000)
cos_σ1 = Array{Float64}(undef, 2000, 2000)
E = Array{Float64}(undef, 30000)

E_real = Array{Float64}(undef, 30000)
B_real = Array{Float64}(undef, 30000)

v = Array{Float64}(undef, 2000)
cos_θ = Array{Float64}(undef, 2000)
sin_θ = Array{Float64}(undef, 2000)
β = Array{Float64}(undef, 2000)

cos_ξ = Array{Float64}(undef, 2000, 2000)
dE = Array{Float64}(undef, 2000)

B = Array{Float64}(undef, 2000)
ζ = Array{Float64}(undef, 2000)

sin_α = Array{Float64}(undef, 2000, 2000)
cos_α = Array{Float64}(undef, 2000, 2000)
cos_χ = Array{Float64}(undef, 2000, 2000)

u_θ = Array{Float64}(undef, 2000)
cos_σ = Array{Float64}(undef, 2000, 2000)
Ð = Array{Float64}(undef, 2000, 2000)

T_g = Array{Float64}(undef, 23)
T_eff_real = Array{Float64}(undef, 2000)

wwf_T = Array{Float64}(undef, 2000)
tcf_T = Array{Float64}(undef, 2000)
lum = Array{Float64}(undef, 23)
w = Array{Float64}(undef, 23)
fc = Array{Float64}(undef, 23)

w_ig = Array{Float64}(undef, 23)
T_c_ig = Array{Float64}(undef, 23)
w_g = Array{Float64}(undef, 9)

flux_rel = [0.10, 0.15, 0.20, 0.30, 0.40, 0.50, 0.55, 0.60,
        0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 0.98,
        1.00, 1.02, 1.04, 1.06, 1.08, 1.10, 1.12]
flux_type = Dict(
    0.10 => 1, 
    0.15 => 2, 
    0.20 => 3, 
    0.30 => 4,
    0.40 => 5,
    0.50 => 6,
    0.55 => 7,
    0.60 => 8,
    0.65 => 9,
    0.70 => 10,
    0.75 => 11,
    0.80 => 12,
    0.85 => 13,
    0.90 => 14,
    0.95 => 15,
    0.98 => 16,
    1.00 => 17,
    1.02 => 18,
    1.04 => 19,
    1.06 => 20,
    1.08 => 21,
    1.10 => 22,
    1.12 => 23,
)
log_g = [13.7, 13.85, 14.0, 14.15, 14.3, 14.45, 14.6, 14.75, 14.9]

c::Float64 = 2.997925e10
G::Float64 = 6.67259e-8
σ_SB::Float64 = 5.67051e-5
k_B::Float64 = 1.3805e-16
h_pl::Float64 = 6.62607015e-27
erg_in_keV::Float64 = 1.602177e-9

#массив
N_l = 23

key_w, key_l, T_c, w_b, ω_rot, cos_i, sin_i, σ_T, z_sch_0, Flux_edd, Lum_obs,surf_0, M_NS, R_eq, b1, b2, b3, lum_0 = args

##################################################
# metric parameters
##################################################
# Расчет метрических параметров
# Two dimensionless parameters:
χ = G * M_NS / (R_eq * c^2) #dze (II.eq.A.1)
Ω = ω_rot * sqrt(R_eq^3 / (G * M_NS)) #epsm (II.eq.A.1)

# plus two dimensionless coefficients:
q_c = -0.11 * (Ω / χ)^2 #q (II.eq.A.8)
b_c = 0.4454 * Ω^2 * χ #bet (II.eq.A.9)

# Moment of inertia
# dimensionless
i_m = sqrt(χ) * (1.136 - 2.53 * χ + 5.6 * χ^2) #(II.eq.A.11)

# dimensionful
I = i_m * M_NS * R_eq^2  #tin (II.eq.A.10-A.11)

N_θ = 30
N_2θ = 2 * N_θ

N_φ = 120  # Были также упомянуты значения 2000 и 128
N_f = 500
N_fm1 = N_f - 1
dθ = 90.0 * (π / 180.0) / Float64(N_θ)
dφ = 2.0 * π / Float64(N_φ)

##################################################
# Photon energy grid
##################################################

# Заполнение сетки энергий фотонов
E[1] = 0.1 #keV
E[N_f] = 50.0 #keV
dlog_E = (log10(E[N_f]) - log10(E[1])) / Float64(N_fm1)

for ν = 2:(N_f-1)
    log_E = log10(E[1]) + dlog_E * Float64(ν - 1)
    E[ν] = exp10(log_E)
end

# Заполнение сетки изменений энергий 
dE[1] = (E[2] - E[1]) / 2.0
dE[N_f] = (E[N_f] - E[N_f - 1]) / 2.0

for ν = 2:(N_f - 1)
    dE[ν] = (E[ν + 1] - E[ν - 1]) / 2.0
end



##################################################
# Coefficients for R(θ) finding
##################################################
# Коэффициенты для R(θ) нахождения
ΩΩ = Ω^2
a0 = -0.18 * ΩΩ + 0.23 * χ * ΩΩ - 0.05 * ΩΩ^2
a2 = -0.39 * ΩΩ + 0.29 * χ * ΩΩ + 0.13 * ΩΩ^2
a4 = 0.04 * ΩΩ - 0.15 * χ * ΩΩ + 0.07 * ΩΩ^2

##################################################
# approximation from AlGendy & Morsink 2014
##################################################
g0 = (G * M_NS / R_eq^2) / sqrt(1.0 - 2.0 * χ) # (II.eq.A.15)

ce = 0.776 * χ - 0.791 # (II.eq.A.16)
cp = 1.138 - 1.431 * χ # (II.eq.A.17)

de = (2.431 * χ - 1.315) * χ
dp = (0.653 - 2.864 * χ) * χ

fe = -1.172 * χ
fp = 0.975 * χ

d60 = (13.47 - 27.13 * χ) * χ
f60 = 1.69

ΩΩ = Ω^2

# Вычисление θ, cos_θ и sin_θ
for j = 1:N_θ
    θ[j] = dθ / 2.0 + dθ * float(j-1)

    # approximation from AlGendy & Morsink 2014 
    cos_θ[j] = cos(θ[j])
    sin_θ[j] = sin(θ[j])

    # approximation from Morsink et all. 2007

    # Legendre's polynomials:
    P_0(cosθ) = 1.0
    P_2(cosθ) = (3.0 * cosθ^2 - 1.0) / 2.0
    P_4(cosθ) = ((35.0 * cosθ^2 - 30.0) * cosθ^2 + 3.0) / 8.0

    # Derivative of Legendre's polynomials
    dP_2(cosθ, sinθ) = -3.0 * cosθ * sinθ
    dP_4(cosθ, sinθ) = 2.5 * (3.0 - 7.0 * cosθ^2) * cosθ * sinθ

    R_θ[j] = R_eq * (1.0 + P_0(cos_θ[j]) * a0 + P_2(cos_θ[j]) * a2 + P_4(cos_θ[j]) * a4)
    u_θ[j] = (2.0 * G * M_NS / c^2) / R_θ[j]

    ##################################################
    # Calculation dR/dθ 
    ##################################################
    dR_θ[j] = R_eq * (a2 * dP_2(cos_θ[j], sin_θ[j]) + a4 * dP_4(cos_θ[j], sin_θ[j]))
    
    ##################################################
    # patch for the spherical rotated NS with R=R_eqv
    # R_θ[j] = R_eq
    ##################################################

    r_i::Float64 = R_θ[j]
    P2_cosθ = P_2(cos_θ[j])
    ū_i, r̄_i = 0.0, 0.0

    converged = false
    while !converged
        r̄_i = r_i
        ū_i = (G * M_NS / c^2) / r̄_i

        ν_0 = log((1.0 - ū_i/2.0) / (1.0 + ū_i/2.0))
        B_0 = (1.0 - ū_i/2.0) * (1.0 + ū_i/2.0)

        ν_i = ν_0 + (b_c/3.0 - q_c * P2_cosθ) * ū_i^3
        B_i = B_0 + b_c * ū_i^2

        r_i = R_θ[j] / exp(-ν_i) / B_i

        errs = abs(r_i - r̄_i) / r_i

        if errs < 1e-8
            r̄ = r_i
            converged = true   
        end
    end
    r̄ = r_i

    ū = (G * M_NS / c^2) / r̄ # (II.eq.A.5)

    # (II.eq.A.4)
    ζ_0 = log(B_0)
    v_0 = log((1.0 - ū/2.0) / (1.0 + ū/2.0))
    B_0 = (1.0 - ū/2.0) * (1.0 + ū/2.0)

    # (II.eq.A.7)
    v[j] = v_0 + (b_c/3.0 - q_c * P_2(cos_θ[j])) * ū^3
    B[j] = B_0 + b_c * ū^2
    ζ[j] = ζ_0 + b_c * ū^2 * (4.0 * P_2(cos_θ[j]) - 1.0) / 3.0
    
    J = I * ω_rot # angular momentum (II.eq.A.10-11)

    ϖ = (2.0 * G * J) / (c^2 * r̄^3) * (1.0 - 3.0 * ū) # (II.eq.A.10)
    ##################################################
    # patch for the spherical rotated NS with R=R_eqv
    # ϖ = 0.0
    ##################################################  

    # phenomenological factor
    β_ph[j] = R_θ[j] * ϖ / c * exp(-v[j]) * sin_θ[j] # (II.eq.B.36)


    ##################################################
    # approximation for the gravity from AlGendy & Morsink 2014
    ################################################## 
    ee = (ce + (de + fe * ΩΩ) * ΩΩ) * ΩΩ
    ep = (cp + (dp - d60 + (fp - f60) * ΩΩ) * ΩΩ) * ΩΩ
    ep = (cp + (dp - d60 + fp * ΩΩ) * ΩΩ) * ΩΩ

    gtet = 1.0 + ee * sin_θ[j]^2 + ep * cos_θ[j]^2 + ( d60 + f60 * ΩΩ ) * ΩΩ^2 *cos_θ[j]
    #gtet1 = 1.0 + (ce * sin_θ[j]^2 + cp * cos_θ[j]^2) * ΩΩ

    ##################################################
    # patch for the spherical rotated NS with R=R_eqv
    # dR_θ[j] = 0.0
    ##################################################  

    f_θ =  B[j] * exp(-ζ[j] - v[j]) * 1/R_θ[j] * dR_θ[j] # ??? (II.eq.B.25)

    ##################################################
    # Schwarzschild approximation
    ##################################################

    # (II.eq.B.24)
    cos_η[j] = 1 / sqrt(1 + f_θ^2)
    sin_η[j] = f_θ / sqrt(1 + f_θ^2)
    if key_w=="null"
        W[j] = 0
    elseif key_w=="const"
        W[j] = 0.75
    elseif key_w=="line"
        θ_max = 30 * π/180
        W[j] = (1 - θ[j]/θ_max) * hs(1 - θ[j]/θ_max)
    end

    grv[j] = gtet * g0 * (1 - W[j]^2)
    β[j] = R_θ[j] * exp(-v[j]) * (ω_rot - ϖ) * sin_θ[j] / c # (II.eq.B.37)

    ##################################################
    # patch for the spherical rotated NS with R=R_eqv
    # R_sch = 2.0 * G * M_NS / c^2 # (II.eq.B.2-3)
    # β[j] = R_θ[j] * ω_rot / c * sin_θ[j] / sqrt(1.0 - R_sch/R_θ[j]) # (II.eq.B.15)
    ##################################################  

    γ[j] = 1.0 / sqrt(1.0 - β[j]^2) # (II.eq.B.16)
end

surf = 0.0
dcos_θ = 0.0

# Численное интегрирование площади
for j = 1:N_θ
    if j > 1 && j < N_θ
        dcos_θ = (cos_θ[j-1] - cos_θ[j+1]) / 2.0
    elseif j == 1
        dcos_θ = 1.0 - (cos_θ[j] + cos_θ[j+1]) / 2.0
    elseif j == N_θ
        dcos_θ = (cos_θ[j-1] + cos_θ[j]) / 2.0
    end
    dS_φ[j] = 1 / cos_η[j] * R_θ[j]^2 * dcos_θ 
    surf += dS_φ[j]
end

rpr = sqrt(surf)

println("\n  surf/R_eq^2     rpr             R_eq:")
println(@sprintf(" %13.6e   %13.6e   %13.6e", surf / R_eq^2, rpr, R_eq))
println("\n         L/L_Edd         T_eff (keV)     f_c             w")

# Заполнение массива значениями
for i = 1:N_φ
    φ[i] = dφ / 2.0 + dφ * float(i - 1)
end

# Отражение на южное полушарие
for j = 1:N_2θ
    if j > N_θ
        θ[j] = θ[j-1] + dθ
        cos_θ[j] = cos(θ[j])
        sin_θ[j] = sin(θ[j])

        jj = N_2θ - j + 1

        v[j] = v[jj]
        R_θ[j] = R_θ[jj]

        cos_η[j] = cos_η[jj]
        sin_η[j] = sin_η[jj]

        dS_φ[j] = dS_φ[jj]
        grv[j] = grv[jj]
        dR_θ[j] = dR_θ[jj]

        β[j] = β[jj]
        γ[j] = γ[jj]

        β_ph[j] = β_ph[jj]
        u_θ[j] = u_θ[jj]
    end
end

for j = 1:N_2θ
    for i = 1:N_φ
        # (II.eq.B.3)
        cos_ψ = cos_i * cos_θ[j] + sin_i * sin_θ[j] * cos(φ[i])
        sin_ψ = sqrt(1.0 - cos_ψ^2)
        ##################################################
        # patch for the spherical rotated NS with R=R_eqv
        
        # (II.eq.B.7-8)
        y = 1.0 - cos_ψ 
        # (II.eq.B.8)
        G_yu = 1.0 + (u_θ[j] * y)^2 / 112.0 - ℯ * u_θ[j] * y / 100.0 * (log(1.0 - 0.5 * y) + 0.5 * y) 
        
        # dxdy (II.eq.B.9)
        Ð[i, j] = 1.0 + 3.0 * (u_θ[j] * y)^2 / 112.0 - ℯ * u_θ[j] * y / 100.0 * (2.0 * log(1.0 - 0.5 * y) + y * (1.0 - 0.75 * y) / (1.0 - 0.5 * y))
        
        # (II.eq.B.7)
        cos_α[i, j] = 1.0 - (1.0 - cos_ψ) * (1.0 - u_θ[j]) * G_yu
        ##################################################
        sin_α[i, j] = sqrt(1.0 - cos_α[i, j]^2)

        # (II.eq.B.30)
        cos_χ[i, j] = (cos_i - cos_θ[j] * cos_ψ) / (sin_θ[j] * sin_ψ)
        
        # (II.eq.B.29)
        cos_σ[i, j] = cos_η[j] * cos_α[i, j] + sin_η[j] * sin_α[i, j] * cos_χ[i, j] * cos_θ[j] / abs(cos_θ[j])
        
        # (II.eq.B.17)
        cos_ξ[i, j] = -1.0 * sin_α[i, j] * sin_i * sin(φ[i]) / sin_ψ

        # (II.eq.B.14)
        δ[i, j] = 1.0 / (γ[j] * (1.0 - β[j] * cos_ξ[i, j]))

        # (II.eq.B.32)
        cos_σ1[i, j] = cos_σ[i, j] * δ[i, j]
    end
end

j_flux = flux_type[lum_0]
println(flux_rel[j_flux],lum_0)
for iif = j_flux:j_flux
    flux_i = flux_rel[iif]
    T_eff_0 = (flux_i*Flux_edd/σ_SB)^(1//4)  # in Kelvin
    θ_eff_0 = (k_B * T_eff_0) / erg_in_keV / z_sch_0 # in keV
    for j = 1:N_θ
        Flux_edd_real = abs(grv[j]) * c / σ_T
        if key_l == 1
            T_eff_real[j] = (flux_i*Flux_edd_real/σ_SB)^(1//4)
        else
            T_eff_real[j] = T_eff_0 
            flux_i = σ_SB * T_eff_real[j]^4 / Flux_edd_real
            if flux_i >= flux_rel[N_l]
                N_l = iif - 1
                @info """incorrectly flux
                flux_i  = $(@sprintf("%.2f", flux_i))
                flux_rel[N_l] = $(@sprintf("%.2f", flux_rel[N_l]))
                flux_i ≥ flux_rel[N_l] = $(@sprintf("%.2f", flux_i))≥$(@sprintf("%.2f", flux_rel[N_l])) = true
                """
                continue
            end
        end

        # Вычисление log_g_real
        log_g_real = log10(abs(grv[j]))

        ##################################################
        # tc, wfc, and w interpolation
        ##################################################

        for ig = 1:9
            # Внутренний цикл для заполнения T_c_ig и w_ig
            for i = 1:N_l
                T_c_ig[i] = T_c[i, ig]
                w_ig[i] = w_b[i, ig]
                
            end

            # Вызов функции интерполяции для T_c
            idum, T_i = map1(flux_rel, T_c_ig, N_l, flux_i, 1)  # Предполагаем, что map1 возвращает пару значений
            T_g[ig] = T_i[1]
            
            # Вызов функции интерполяции для w
            idum, w_i = map1(flux_rel, w_ig, N_l, flux_i, 1)  # Аналогичное предположение для вывода функции map1
            w_g[ig] = w_i[1]
        end

        idum, T_f = map1(log_g,T_g,9,log_g_real,1)
        idum, w_f = map1(log_g,w_g,9,log_g_real,1)  
        
        wwf_T[j] = w_f[1]
        tcf_T[j] = T_f[1]
    end

     
    ##################################################
    # Calculation of all the necessary angles 
    ##################################################
    for j = 1:N_2θ
        if (j > N_θ)
            jj=N_2θ - j + 1
            tcf_T[j]=tcf_T[jj]
            wwf_T[j]=wwf_T[jj]
        end
    end
    
    ##################################################
    # Calculation of the rotating NS spectrum
    ##################################################
    
    surf_real = 0.0
    Lum = 0.0

    for ν = 1:N_f
        B_real[ν] = 0.0
        for j = 1:N_2θ
            for i = 1:N_φ
                if (cos_σ[i,j] < 0.0)
                    continue
                end

                # (II.eq.B.35)
                E_real[ν] = E[ν] / (exp(v[j]) * δ[i,j]) / (1.0 + β_ph[j] * cos_ξ[i,j])
                
                ##################################################
                # Schwarzschild approximation

                # Функция Планка
                # Непонятный фактор
                ϰ = 5.06384420694559
                ε_ν = 1/ϰ * 2*π / (c * h_pl)^2 * erg_in_keV * E_real[ν]^3 / (exp(E_real[ν] / tcf_T[j]) - 1.0)
                
                # (II.eq.9)
                I_E = wwf_T[j] * ε_ν * (0.4215 + 0.86775 * cos_σ1[i,j])
                
                # (II.eq.B.35)
                E_to_E_real = (exp(v[j]) * δ[i,j]) * (1.0 + β_ph[j] * cos_ξ[i,j])
                   
                # (II.eq.B.33)
                dΩ_obs = cos_σ[i,j] * dS_φ[j] *dφ * Ð[i,j]   


                # (II.eq.B.34)
                B_real[ν] += E_to_E_real^3 * I_E * dΩ_obs

                ##################################################
                # computation of the NS projection area
                if (ν==1) 
                    surf_real += dΩ_obs
                end
                ##################################################
            end
        end

        Lum += B_real[ν]*dE[ν]  
    end

    lum[iif] = 4.0 * π * Lum / 10.0^36 #/ Lum_obs

    w_min = 0.01
    w_max = 1.05
    N_w = 100
         
    fc_min = 1.0e-1
    fc_max = 3.0
    N_fc = 100
        
    iter = 0

    while !(iter>4)
        contsum = 1.e50
        dw = (w_max - w_min) / Float64(N_w)
        dfc = (fc_max - fc_min) / Float64(N_w)    

        iter += 1

        for i=1:N_w
            w_real = w_min + dw*Float64(i-1)
            for j=1:N_fc
                fc_real = fc_min + dfc*Float64(j-1)
                cont = 0.0
                for ν=1:N_f
                    if (E[ν]>3.0 && E[ν]<20.0)
                        # Функция Планка
                        # Непонятный фактор
                        ϰ = 5.06384420694559
                        ε = 1/ϰ * 2*π / (c * h_pl)^2 * erg_in_keV * E[ν]^3 / (exp(E[ν] / (fc_real*θ_eff_0)) - 1.0)
                        fc_ν = w_real * π * ε / E[ν]
                        cont += (fc_ν - B_real[ν] / surf_0 / E[ν])^2
                    end  
                end
                if (cont<contsum)
                    fc[iif] = fc_real
                    w[iif] = w_real
                    contsum = cont
                end
            end
        end

        w_min = w[iif] - 2.0 * dw
        w_max = w[iif] + 2.0 * dw
        fc_min = fc[iif] - 2.0 * dfc
        fc_max = fc[iif] + 2.0 * dfc
    end
    println(@sprintf("%4i  %14.5e  %14.5e  %14.5e  %14.5e", iif, lum[iif], θ_eff_0, fc[iif], w[iif]))
end

X, Y = E[1:N_f], 10.0^(-36) .* B_real[1:N_f] 
return X, Y

end

function hs(x)
	0.5 * (sign(x) + 1)
end

function f_30(X, F, N, l)
    l = min(N,l)                                                  
    c = 0.0                                                             
    b = (F[l]-F[l-1])/(X[l]-X[l-1])                        
    a = F[l]-X[l]*b                                            
    ll = l
    return a, b, c, l, ll
end

function f_21(X, F, N, l, l1)
    l2 = l - 2                                                          
    d = (F[l1] - F[l2]) / (X[l1] - X[l2])
    c1 = F[l]/((X[l]-X[l1])*(X[l]-X[l2])) + (F[l2]/(X[l]-X[l2]) - F[l1]/(X[l]-X[l1]))/(X[l1]-X[l2]) 
    b1 = d - (X[l1] + X[l2]) * c1                                 
    a1 = F[l2] - X[l2] * d + X[l1] * X[l2] * c1  
    return a1, b1, c1, l2
end

function f_25(X, F, N, l, l1)
    d = (F[l]-F[l1])/(X[l]-X[l1])                          
    cf = F[l+1]/((X[l+1]-X[l])*(X[l+1]-X[l1])) + (F[l1]/(X[l+1]-X[l1]) - F[l]/(X[l+1]-X[l]))/(X[l]-X[l1])   
    bf = d - (X[l] + X[l1]) * cf                                   
    af = F[l1]-X[l1]*d + X[l]*X[l1]*cf 
    return af, bf, cf
end

function f_26(a1, b1, c1, af, bf, cf, l)
    WT = 0.0
    if (abs(cf)!=0.0)   
        WT = abs(cf)/(abs(cf)+abs(c1))
    end
    a = af + WT*(a1-af)                                            
    b = bf + WT*(b1-bf)                                            
    c = cf + WT*(c1-cf)                                            
    ll = l  
    return a, b, c, ll
end


function map1(X, F, N, Y, M)
    G = Array{Float64}(undef, M)
    l, ll, l1 = 2, 0, 0

    a, b, c = 1.0, 1.0, 1.0
    a1, b1, c1 = 1.0, 1.0, 1.0
    af, bf, cf = 1.0, 1.0, 1.0

    y = Y[1]
    
    while !(y < X[l])
        l += 1
        if (l > N) 
            break
        end
    end

    if (l > N) || (l == 2)
        a, b, c, l, ll = f_30(X, F, N, l)

    else
        l1 = l - 1
        a1, b1, c1 = f_21(X, F, N, l, l1)
        if (l != N)    
            af, bf, cf = f_25(X, F, N, l, l1)    
            a, b, c, ll = f_26(a1, b1, c1, af, bf, cf, l)       
        else
            a, b, c, ll = a1, b1, c1, l                         
        end
    end

    G[1] = a + b*y + c*y^2
    return (ll-1, G)  
end

function map(X, F, N, Y, M)
    G = Array{Float64}(undef, M)
    l, ll, l1, l2 = 2, 0, 0, 0
    a, b, c = 1.0, 1.0, 1.0
    a1, b1, c1 = 1.0, 1.0, 1.0
    af, bf, cf = 1.0, 1.0, 1.0

    for k=1:M
        y = Y[k]
        while !(y < X[l])
            l += 1
            if (l > N)
                if (l != ll)
                    a, b, c, l, ll = f_30(X, F, N, l)
                end
                break
            end
        end

        if (l != ll) && (l != 2)
            l1 = l - 1 
            if (l > (ll + 1) || l == 3)
                a1, b1, c1, l2 = f_21(X, F, N, l, l1)

                if (l >= N)    
                    a, b, c, l, ll = a1, b1, c1, l, l    
                else 
                    af, bf, cf = f_25(X, F, N, l, l1) 
                    a, b, c, ll = f_26(a1, b1, c1, af, bf, cf, l)                                               
                end
            else 
                c1 = cf                                                       
                b1 = bf                                                       
                a1 = af

                if (l==N)
                    a, b, c, l, ll = f_22(X, F, N, l)
                else
                    af, bf, cf = f_25(X, F, N, l, l1)    
                    a, b, c, ll = f_26(a1, b1, c1, af, bf, cf, l)   
                end

            end

        elseif (l != ll) && (l == 2)
            a, b, c, l, ll = f_30(X, F, N, l)
        end

        G[k] = a + b*y + c*y^2
    end
    return (ll-1, G)  
end