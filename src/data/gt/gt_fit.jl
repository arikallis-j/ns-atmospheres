function fit_gt(args)
# Определение массивов
θ = Array{Float64}(undef, 2000)
W = Array{Float64}(undef, 2000)
grv_norm = Array{Float64}(undef, 2000)
cos_θ = Array{Float64}(undef, 2000)
sin_θ = Array{Float64}(undef, 2000)

c::Float64 = 2.997925e10
G::Float64 = 6.67259e-8


key_w, a1, a2, a3, ω_rot, a4, a5, a6, a7, a8, a9, a10, M_NS, R_eq, a11, a12, a13, a14 = args

##################################################
# metric parameters
##################################################
# Расчет метрических параметров
# Two dimensionless parameters:
χ = G * M_NS / (R_eq * c^2) #dze (II.eq.A.1)
Ω = ω_rot * sqrt(R_eq^3 / (G * M_NS)) #epsm (II.eq.A.1)
g_0 = (G * M_NS / R_eq^2) * (1 - 2*χ)^(-1/2) # (II.eq.A.15)

N_θ = 30
dθ = 90.0 * (π / 180.0) / Float64(N_θ)

##################################################
# approximation from AlGendy & Morsink 2014
##################################################
ce = 0.776 * χ - 0.791 # (II.eq.A.16)
cp = 1.138 - 1.431 * χ # (II.eq.A.17)

de = (2.431 * χ - 1.315) * χ
dp = (0.653 - 2.864 * χ) * χ

fe = -1.172 * χ
fp = 0.975 * χ

d60 = (13.47 - 27.13 * χ) * χ
f60 = 1.69

ΩΩ = Ω^2

N_θ = 30
dθ = 90.0 * (π / 180.0) / Float64(N_θ)

# Вычисление θ, cos_θ и sin_θ
for j = 1:N_θ
    θ[j] = dθ / 2.0 + dθ * float(j-1)
    cos_θ[j] = cos(θ[j])
    sin_θ[j] = sin(θ[j])

    ##################################################
    # approximation for the gravity from AlGendy & Morsink 2014
    ################################################## 
    ee = (ce + (de + fe * ΩΩ) * ΩΩ) * ΩΩ
    ep = (cp + (dp - d60 + (fp - f60) * ΩΩ) * ΩΩ) * ΩΩ
    ep = (cp + (dp - d60 + fp * ΩΩ) * ΩΩ) * ΩΩ

    gtet = 1.0 + ee * sin_θ[j]^2 + ep * cos_θ[j]^2 + ( d60 + f60 * ΩΩ ) * ΩΩ^2 *cos_θ[j]

    if key_w=="null"
        W[j] = 0
    elseif key_w=="const"
        W[j] = 0.75
    elseif key_w=="line"
        θ_max = 30 * π/180
        W[j] = (1 - θ[j]/θ_max) * hs(1 - θ[j]/θ_max)
    end

    grv_norm[j] = gtet * (1 - W[j]^2)
end

X, Y = 180/π .* θ[1:N_θ], grv_norm[1:N_θ]
return X, Y

end

function hs(x)
	0.5 * (sign(x) + 1)
end