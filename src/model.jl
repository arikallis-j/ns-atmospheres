#constant
#c - скорость света в вакууме, 2.997925 × 10^10 см/с.
#G - гравитационная постоянная, 6.67259 × 10^-8 см^3/г с^2.
#mₚ - масса протона, 1.67262 × 10^-24 г.
#σ_SB - постоянная Стефана-Больцмана, 5.67051 × 10^-5 эрг см^-2 с^-1 К^-4.
#k_B - постоянная Больцмана, 1.3805 × 10^-16 эрг/К.
#ergkeV - пересчет единиц из эрг в кэВ, 1.602177 × 10^-9 эрг/кэВ.
#M☉ - масса Солнца, 1.991 × 10^33 г.
# numbers of \ell where the w and f_c were tabulated

const c::Float64 = 2.997925e10
const G::Float64 = 6.67259e-8
const σ_SB::Float64 = 5.67051e-5
const k_B::Float64 = 1.3805e-16
const erg_in_keV::Float64 = 1.602177e-9
const M_☉::Float64 = 1.991e33
const nll = 23

function sp_rot_NS(dim, parameters, args)
    is_save = args["par"]

    fit_par = parameters["fit_par"]
    sys = fit_par["sys"]
    type = args["type"] == "" ? fit_par["type"] : args["type"]
    w_key = args["w_key"] == "" ? fit_par["w_key"] : args["w_key"]
    fc_key = args["kfc"] == 0.0 ? fit_par["kfc"] : args["kfc"]
    flux_key = args["kfl"] == 0.0 ? fit_par["kfl"] : args["kfl"]

    ns_par = parameters["ns_par"]

    m_ns = args["m_ns"] == 0.0 ? ns_par["m_ns"] : args["m_ns"]
    r_ns = args["r_ns"] == 0.0 ? ns_par["r_ns"] : args["r_ns"]
    m_cor_0 = args["m_cor"] == 0.0 ? ns_par["m_cor"] : args["m_cor"]
    r_eq_0 = args["r_eq"] == 0.0 ? ns_par["r_eq"] : args["r_eq"]
    ν_rot = args["v_rot"] == 0.0 ? ns_par["v_rot"] : args["v_rot"]
    incl_angle = args["i_ang"] == 0.0 ? ns_par["i_ang"] : args["i_ang"]
    lum = args["lum"] == 0.0 ? ns_par["lum"] : args["lum"]

    pnt_par = parameters["pnt_par"]
    
    theta = args["theta"] == 0.0 ? pnt_par["theta"] : args["theta"]
    phi = args["phi"] == 0.0 ? pnt_par["phi"] : args["phi"]


    r_min = 2.95 * m_ns * 1.5
    if r_ns <= r_min
        println("causility error")
        par = @sprintf("%.5f %.5f %.5f", m_ns, r_ns, r_min)
        println(par)
        exit()
    else
        @debug """correctly radius
        r_min = $(@sprintf("%.5f", r_min)),
        r_ns = $(r_ns)
        r_ns < r_min = $(r_ns)<$(@sprintf("%.5f", r_min)) = true
        """
    end

    M_NS = m_ns * M_☉
    R_NS = r_ns * 1e5 > r_min ? r_ns * 1e5 : 0.0
    R_sch = 2.0 * G * M_NS / c^2
    zst = 1.0 / sqrt(1.0 - R_sch / R_NS)
    g = (G * M_NS / R_NS^2) * zst
    log_g = log10(g)
    surf_0 = (R_NS * zst)^2

    X_hyd::Float64 = type=="he" ? 0.0 : 0.7374
    σ_T::Float64 = 0.2 * (1.0 + X_hyd)
    w_b = Array{Float64}(undef, 23, 9)
    T_c = Array{Float64}(undef, 23, 9)

    if type=="s001"
        file = open("src/spectra/fcol_S001.dat", "r")
    elseif type=="s1"
        file = open("src/spectra/fcol_S1.dat", "r")
    elseif type=="he"
        file = open("src/spectra/fcol_He.dat", "r")
    end
    spectrum = readlines(file)
    for ig in 1:9
        for j in 1:nll
            gap = type=="s1" ? 2 : 1
            line = convert(Array{String},split(spectrum[(ig-1)*nll+j], " ")[gap:end])
            if fc_key == 1
                a1, a2, gr, T_eff, f_c, a4, a5, a6, a7, w_fc = parse.(Float64, line)
            else
                a1, a2, gr, T_eff, a3, f_c, a4, a5, a6, a7, w_fc = parse.(Float64, line)
            end
            # Расчет величин
            w_b[j, ig] = w_fc * f_c ^ (-4)
            T_c[j, ig] = T_eff * f_c
        end
    end
    close(file)


    Flux_edd = g * c / σ_T
    T_edd = (Flux_edd/σ_SB)^(1//4) / zst
    θ_edd = k_B * T_edd
    ε_edd = θ_edd / erg_in_keV
    Lum_edd = 4.0 * π * R_NS^2 * Flux_edd
    Lum_obs = Lum_edd / zst^2


    ω_rot = 2.0*π*ν_rot
    i_angle = π * incl_angle/180.0
    cos_i = cos(i_angle)
    sin_i = sin(i_angle)

    ν_cr::Float64 = 1278.0 * (10.0/r_ns)^1.5 * sqrt(m_ns/1.4)
    ν_rel::Float64 = ν_rot / ν_cr 

    r_1 = 0.025 
    r_2 = 0.07 * (m_ns/1.4)^1.5
    if r_eq_0==0
        r_eq = r_ns * (0.9766 - r_1 / (ν_rel - 1.07) + r_2 * ν_rel^2)
    else
        r_eq = r_eq_0
    end
    R_eq = r_eq * 1e5
    
    a_1 = 0.001 * (m_ns/1.4)^1.5
    a_2 = 10.0 * a_1
    a_0 = 1.0 - a_1/1.1
    if m_cor_0==0
        m_cor = m_ns * (a_0 - a_1 / (ν_rel - 1.1) + a_2 * ν_rel^2)
    else
        m_cor = m_cor_0
    end
    M_cor = m_cor * M_☉
    
    #println("\n  rel_spin        R_eqv (cm)      M_corr (M_sun)")
    #println(@sprintf(" %13.6e   %13.6e   %13.6e", NS.ν_rel, NS.R_eq, NS.m_cor))


    # Вывод значений zst, glg1, tefst.
    #println("  M (M_sun)       R(km)           spin (Hz)       incl (deg)")
    #println(@sprintf("%13.5e  %13.4e   %13.4e   %13.4e", m_ns, r_ns, ν_rot, incl_angle))
    #println("  1+z             log g           T_Edd (keV)")
    #println(@sprintf("%12.4e    %12.4e    %12.4e", zst, log_g, ε_edd))

    # Вместо метки 1001 используется форматирование строки в Julia
    if is_save
        NS = YAML.load_file("src/data/ns/ns_config.yml")
        NS["parameters"]["fit_par"]["sys"] = sys
        NS["parameters"]["fit_par"]["type"] = type
        NS["parameters"]["fit_par"]["fc_key"] = fc_key
        NS["parameters"]["fit_par"]["flux_key"] = flux_key
        NS["parameters"]["fit_par"]["w_key"] = w_key

        NS["parameters"]["ns_par"]["m_ns"] = m_ns
        NS["parameters"]["ns_par"]["r_ns"] = r_ns
        NS["parameters"]["ns_par"]["ν_rot"] = ν_rot
        NS["parameters"]["ns_par"]["incl_angle"] = incl_angle
        NS["parameters"]["ns_par"]["lum"] = lum

        NS["parameters"]["pnt_par"]["theta"] = theta
        NS["parameters"]["pnt_par"]["phi"] = phi

        NS["properties"]["phisical"]["M_NS"] = M_NS
        NS["properties"]["phisical"]["R_NS"] = R_NS
        NS["properties"]["phisical"]["R_sch"] = R_sch
        NS["properties"]["phisical"]["log_g"] = log_g
        NS["properties"]["phisical"]["log_g"] = zst
        NS["properties"]["phisical"]["surf_0"] = surf_0

        NS["properties"]["chemical"]["X_hyd"] = X_hyd
        NS["properties"]["chemical"]["σ_T"] = σ_T
        NS["properties"]["chemical"]["spectra"]["w_b"] = w_b
        NS["properties"]["chemical"]["spectra"]["T_c"] = T_c

        NS["properties"]["photometrical"]["Flux_edd"] = Flux_edd
        NS["properties"]["photometrical"]["T_edd"] = T_edd
        NS["properties"]["photometrical"]["θ_edd"] = θ_edd
        NS["properties"]["photometrical"]["ε_edd"] = ε_edd
        NS["properties"]["photometrical"]["Lum_edd"] = Lum_edd
        NS["properties"]["photometrical"]["Lum_obs"] = Lum_obs

        NS["properties"]["rotational"]["ω_rot"] = ω_rot
        NS["properties"]["rotational"]["i_angle"] = i_angle
        NS["properties"]["rotational"]["cos_i"] = cos_i
        NS["properties"]["rotational"]["sin_i"] = sin_i

        NS["properties"]["relativistic"]["ν_cr"] = ν_cr
        NS["properties"]["relativistic"]["ν_rel"] = ν_rel
        NS["properties"]["relativistic"]["r_eq"] = r_eq
        NS["properties"]["relativistic"]["R_eq"] = R_eq
        NS["properties"]["relativistic"]["m_cor"] = m_cor
        NS["properties"]["relativistic"]["M_cor"] = M_cor
        YAML.write_file("src/data/ns/ns_$(sys)_$(type).yml", NS)
    end

    args = w_key, flux_key, T_c, w_b, ω_rot, cos_i, sin_i, σ_T, zst, Flux_edd, Lum_obs, surf_0, M_cor, R_eq, R_sch, theta, phi, lum
    X, Y = fits[dim](args)
    return X, Y
end