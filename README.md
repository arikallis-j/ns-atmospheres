# Installation of Julia
For using this program, you need to install Julia

There is simple way to do it via package [jill.py](https://github.com/johnnychen94/jill.py). Install it by pip-installer:
```shell
$ pip3 install jill
```
After it, install julia in version that we need (nowdays the latest is 1.10.4)
```shell
$ jill install 1.10.4
```
Check that all is working via command:
```shell
$ jill list
julia       	-->	1.10.4
julia-1     	-->	1.10.4
julia-1.10  	-->	1.10.4
```

# Installation of Libraries
Using JuliaREPL, adding all packages that we need:
```shell
$ julia
julia> ]
(@v1.10) pkg> add Plots
```

Packages, that used in this program:
+ Comonicon - for CLI-interface
+ YAML - for .yaml data
+ LaTeXStrings - for using LaTeX on graphics
+ Printf - for formatting string in Terminal
+ Plots - for plot pdf-graphics
+ UnicodePlots - for plot graphics in Terminal

# Usage of the program:

## Base options
- `calc`: calculation of the dependenceand after that printing it
- `print`: only printing of the dependence by program data
- `guide`: print greeting or theoretical notes

## The main panel
```shell
$ julia main.jl guide --theory
```

## Caclculate parameters
- `-p, --par`: save parameters of neutron star

- `-t, --type=<str>`: working with one chemical composition
    * type = s001 - solar H/He mix with the heavy element abundances reduced in 100 times
    * type = s1   - solar abundance
    * type = he   - pure helium

- `-d, --dim=<str>`: working with one dependence
    * dim = lfc - lumen to f_c dependence
    * dim = gt  - g/g_0 to θ dependence
    * dim = be  - B_planck to ε dependence
    * dim = fe  - normalized flux to E/E' dependence (NOT WORKING NOW)

- `-s, --sys=<str>`: working with one configuration
    * sys = base - base config
    * sys = fig_a1 - config for figure A.1 in paper arXiv:2005.09759
    * sys = line - config for testing line-dependence of g_eff from θ

- `-g, --graph=<str>`: printing additional elements
    * graph = base  - print all chemical composion
    * graph = log  - print all chemical composion
    * graph = all  - print all chemical composion
    * graph = edge - apply edge-line to graph

```shell
$ julia main.jl calc --dim=lfc -t=he -s=base -g=all
```

## Fitting parameters
- `--kfc=<int>`: fc-key is the key for the tabulated fitting parameters w and f_c obtaining
    * fc-key = 1 - the energy spectra F_E were fitted
    * fc-key = 2 - the photon spectra F_E / E were fitted

- `--kfl=<int>`: flux-key is the relative or absolute flux over the surface of a neutron star
    * flux-key = 1 - relative flux F/F_Edd
    * flux-key = 2 - absolute flux F

- `-w, --wkey=<str>`: profile of latitudinal velocity, normalized to keplerian velocity
    * w-key = null  - without spread belt
    * w-key = const - decreasion of g_null
    * w-key = line  - firts model of latitudinal velocity

```shell
$ julia main.jl calc -d=lfc -t=s1 --kfc=1 --kfl=0 -w=null
```

## Neutron star parameters
- `-m, --mns=<int>`: mass of neutron star, in solar masses
- `-r, --rns=<int>`: radius of neutron star, in km
- `-v, --vrot=<int>`: rotation frequency of neutron star, in Hz
- `-i, --iang=<int>`: inclination angle between the axis of rotation and the line of sight, in deg
- `--mcor=<int>`: corrected by General Relativity mass of neutron star, in solar masses
- `--req=<int>`: corrected by General Relativity equatorial radius of neutron star , in km
- `--lum=<int>`: relative luminosity, in L_edd

```shell
$ julia main.jl calc -m=1.5 -r=12.0 --vrot=700.0 -i=60.0 
```

```shell
$ julia main.jl calc --mcor=1.8 --req=13.6 --vrot=842.0 -i=70.0 
```

```shell
$ julia main.jl calc --dim=be --lum=0.9 --vrot=700.0 -i=90.0 
```

## Point parameters (NOT WORKING NOW)
- `--theta=<int>`: angle of latitude, in deg
- `--phi=<int>`: angle of longitude, in deg



